#!/bin/bash

XDEBUG_FILE="/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"

if [ -n "$XDEBUG_REMOTE_PORT" -a "$XDEBUG_REMOTE_PORT" -ge 0 ]
then
    sed -i "s/xdebug.remote_port=.*/xdebug.remote_port=$XDEBUG_REMOTE_PORT/" $XDEBUG_FILE
fi

if [ -n "$XDEBUG_REMOTE_HOST" ]
then
    sed -i "s/xdebug.remote_host=.*/xdebug.remote_host=$XDEBUG_REMOTE_HOST/" $XDEBUG_FILE
fi

if [ -n "$XDEBUG_REMOTE_ENABLE" ]
then
    sed -i "s/xdebug.remote_enable=.*/xdebug.remote_enable=$XDEBUG_REMOTE_ENABLE/" $XDEBUG_FILE
fi

if [ -n "$XDEBUG_REMOTE_AUTOSTART" ]
then
    sed -i "s/xdebug.remote_autostart=.*/xdebug.remote_autostart=$XDEBUG_REMOTE_AUTOSTART/" $XDEBUG_FILE
fi

if [ -n "$XDEBUG_REMOTE_CONNECT_BACK" ]
then
    sed -i "s/xdebug.remote_connect_back=.*/xdebug.remote_connect_back=$XDEBUG_REMOTE_CONNECT_BACK/" $XDEBUG_FILE
fi

#cd /src/controltower/
#composer install
#yarn install dev

/usr/sbin/apache2ctl -D FOREGROUND
