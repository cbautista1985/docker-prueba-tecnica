#!/bin/bash

project="redsocial"
DIR="src"

mkdir $DIR
docker build -t $project:dev -f Dockerfile . --build-arg dev=yes
rm -rf $DIR


