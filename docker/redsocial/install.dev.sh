#!/bin/bash
find ~/workspace/redsocial/ -iname *.orig | xargs rm
docker exec -ti $(docker ps -f "name=redsocial" -q) /bin/bash -c 'cd /src/redsocial; composer install; yarn install; yarn encore dev'
